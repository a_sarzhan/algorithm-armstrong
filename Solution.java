package com.javarush.task.task20.task2025;
import java.util.*;
/*
Алгоритмы-числа
*/
public class Solution {
    public static long[] getNumbers(long N) {
        long[] result = null;
        TreeSet<Long> numbers = new TreeSet<>();
        long Nsize = N;
        int sz = 0;
        while(Nsize != 0L){
            Nsize = Nsize / 10;
            sz++;
        }
        final int SIZE = sz + 1;
        long[][] powLst = new long[11][SIZE];
        int[] lst = new int[SIZE];
        //filling pow array
        long sum = 1L;
        for (int i = 2; i < powLst.length; i++) {
            for (int j = 0; j < powLst[i].length; j++) {
                if(j != 0)
                    sum *= i;
                powLst[i][j] = sum;
            }
            sum = 1L;
        }
        label:
        for(long i = 1L; i < N;){

            long x = i;
            int lenCount = 0;
            long current;
            // Assel added below params
            boolean first = false;
            int digit = 0;
            int pos = 0;
            long step = 0L;
            int next = 0;
            /* запись цифр числа в массив lst
             pos - позиция последней !=0 цифры, digit - последняя ненулевая цифра
             next - последняя ненулевая цифра числа
             lenCount - array lst length
             */
            while(x != 0L){
                current = (long)x % 10;
                lst[lenCount] = (int)current;
                x = x / 10;
                if(current != 0L){
                    if(!first){
                        pos = lenCount;
                        first = true;
                        digit = (int) current;
                    }
                    next = (int) current;
                }
                lenCount++;

            }

            // определяю шаг(step) основного цикла for(i<N)
            if(pos+1 == lenCount){      // ненулевая цифра единственная в числе, пример: 400, 7000
                if(lenCount == 1)
                    step = 1L;          // шаг = 1, 1<= i <= 9
                else
                    step = digit;       // шаг = первой ненулевой цифре числа (400) -> 404
            }
            else{
                if(pos == 0){           //ненулевая цифра самая первая, но не единственная
                    step = 1L;          // шаг = 1
                }
                else{                   // ненулвеая цифра не первая
                    if(digit < next){   // если число не в возрастающем порядке, пример (410) - 1<4
                        step = (next - digit) *powLst[10][pos];
                        for(int l = 0; l < pos; l++){
                            step += next * powLst[10][l];
                        }
                        i += step;         //пример: шаг = 34 при i = 410
                        if(i < 0)
                            break;
                        continue label;     // не вычисляя переходим на следующую i  с шагом step
                    }
                    else{                   // число в возрастяющем порядке
                        for(int l = 0; l < pos; l++){
                            step += digit * powLst[10][l];      // шаг = 444 при i = 44000
                        }
                        if(lst[0] == 0){
                            i += step;
                            if(i < 0)
                                break;
                            continue label;                  // последняя цифра = 0, переход на след i (ex: 5670 = 5067 - ранее уже был рассмотрен)
                        }
                    }
                }
            }
            //конец опред шага
            // Алгоритм армстронга
            // 1. равна ли summa1 по формуле= числу i
            long summa1 = 0;
            long a1;
            for (int j = 0; j < lenCount; j++) {
                a1 = lst[j];
                if (a1 != 0 && a1 != 1) {
                    a1 = powLst[lst[j]][lenCount];
                }
                summa1 += a1;
            }
            if(summa1 == i){
                //System.out.println("raven "+summa1 + " num: "+i);
                numbers.add(summa1);
            }
            // 2. цифры найденной суммы summa1 совпадают ли с цифрами числа i (порядок не важен)
            else if(summa1 < N){
                long xx = summa1;
                int len = 0, elemCount = 0;

                while(xx != 0L){
                    current = (long)xx % 10;
                    for(int k = 0; k < lenCount; k++){
                        if((int)current == lst[k]){
                            elemCount++;
                            lst[k] = -1;                // как только найдено совпадающие цифры значение массива меняю на -1
                            break;                      // чтобы для остальных цифер summa1 их повторно не рассматривать
                        }

                    }
                    xx = xx / 10;
                    len++;
                }

                if(len == lenCount && len == elemCount){                    // кло-во найденных цифер равно длине массива
                    numbers.add(summa1);
                }
            }

            i += step;
            if(i < 0)
                break;

        }
        result = new long[numbers.size()];

        int count = 0;
        for (Long l : numbers) {
            result[count] = l;
            count++;
        }
        return result;
    }

    public static void main(String[] args) {
        Long t0 = System.currentTimeMillis();
        long n = Long.MAX_VALUE;
        long[] lst = getNumbers(n);
        Long t1 = System.currentTimeMillis();
        System.out.println("time: " + (t1 - t0) / 1000d + " sec");
        System.out.println("memory: " + (Runtime.getRuntime().totalMemory() - Runtime.getRuntime().freeMemory()) / (1024 * 1024) + " mb");
        for (int i = 0; i < lst.length; i++) {
            System.out.print(lst[i] + ", ");
        }
        System.out.println();
    }
}
